import { makeTypescriptConfig } from '@bojnit/skeleta/typescript'

const config = makeTypescriptConfig({
  //
})

export default {
  json: {
    'tsconfig.json': config,
    '.config/tsconfig.cjs.json': {},
    '.config/tsconfig.esm.json': {},
  },
}
