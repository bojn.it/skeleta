import { makeTypescriptConfig } from '@bojnit/skeleta/typescript'

const typescriptConfig = makeTypescriptConfig({})

export default {
  json: {
    'tsconfig.json': typescriptConfig,
  },
}
